﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NBoxx.Common.Patterns {
    public class TimeCache[T] {
        UpdatePeriod         : TimeSpan;
        compValue            : void -> T;
        waitUpdate           : bool;
        mutable _compRun     : bool;
        mutable _value       : option[T].Some = Some(default(T));
        mutable _lastUpdate  : DateTime       = DateTime.MinValue;
        this(updatePeriod : double, compValue : void -> T, waitUpdate : bool = false) {
            this(updatePeriod |> TimeSpan.FromMilliseconds, compValue, waitUpdate)
        }
        this(updatePeriod : TimeSpan, compValue : void -> T, waitUpdate : bool = false) {
            this.UpdatePeriod = updatePeriod;
            this.compValue    = compValue;
            this.waitUpdate   = waitUpdate;
        }
        Value : T {
            get {
                def now = DateTime.Now;
                when(now - _lastUpdate > UpdatePeriod)
                    lock(_value)
                        when(now - _lastUpdate > UpdatePeriod) {
                            if(waitUpdate || _lastUpdate == DateTime.MinValue)
                                _value = compValue() |> Some;
                            else when(_compRun == false) {
                                _compRun = true;
                                _ = Task.Factory
                                    .StartNew(compValue)
                                    .ContinueWith(r => lock(_value) {
                                        _value      = r.Result |> Some;
                                        _lastUpdate = DateTime.Now;
                                        _compRun    = false;
                                    });
                                }
                            _lastUpdate = DateTime.Now;
                        }
                _value.Value
            }
        }
    }
}
