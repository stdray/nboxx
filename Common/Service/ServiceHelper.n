﻿#pragma indent
using Nemerle
using Nemerle.Collections
using Nemerle.Extensions
using Nemerle.Text
using Nemerle.Utility

using System
using System.Collections.Generic
using System.Configuration.Install
using System.Linq
using System.Reflection
using System.ServiceProcess

namespace NBoxx.Common.Service
    using System.Console
    using Options = ServiceStartOptions
    
    public module ServiceHelper
        public ParseOptions(args : array[string]) : Options
            def opts = Options()
            if(CommandLine.Parser.Default.ParseArguments(args, opts))
                opts
            else
                opts.GetUsage() |> WriteLine
                Options.Default
        
        public InstallOrUninstallService[T](options : Options) : void where T : ServiceBase
            def exePath = typeof(T).Assembly.Location 
            match(options)
                | Options where (Install = true,  Uninstall = false) =>
                    ManagedInstallerClass.InstallHelper(array[exePath])
                | Options where (Install = false, Uninstall = true)  => 
                    ManagedInstallerClass.InstallHelper(array["/u", exePath])
                | Options where (Install = false, Uninstall = false) => 
                    ()
                | _                                                  => 
                    throw ArgumentException(
                        "You can not use the install and uninstall options at the same time",
                        "options");
        
        public RunServiceOrConcole[T](service : T, args : array[string]) : void where T : ServiceBase
            def getMethod(name) 
                service.GetType().GetMethod(name, BindingFlags.Instance | BindingFlags.NonPublic)
            def run  = () => _ = getMethod("OnStart").Invoke(service, array[ args ])
            def stop = () => _ = getMethod("OnStop").Invoke(service, null)
            using(service)
                if(Environment.UserInteractive)
                    Console.CancelKeyPress += _ => stop()
                    run()
                    WriteLine("Running service, press enter to stop.")
                    _ = ReadLine()
                    stop()
                    WriteLine("Service stopped. Goodbye.")
                else
                    ServiceBase.Run(service)
