﻿#pragma indent
using Nemerle
using Nemerle.Collections
using Nemerle.Extensions
using Nemerle.Text
using Nemerle.Utility

using System
using System.Collections.Generic
using System.Configuration.Install
using System.ComponentModel
using System.Linq
using System.ServiceProcess;

namespace NBoxx.Common.Service

    [RunInstaller(true)] \
    public class ServiceInstallerBase : Installer
        serviceProcessInstaller : ServiceProcessInstaller 
        serviceInstaller        : ServiceInstaller
        
        public this(serviceName : string, account = ServiceAccount.LocalSystem)
            serviceProcessInstaller = ServiceProcessInstaller() <-
                Account  = account
                Password = null
                Username = null
            serviceInstaller = ServiceInstaller() <-
                ServiceName = serviceName
                StartType   = ServiceStartMode.Automatic;
            Installers.AddRange(array[serviceProcessInstaller, serviceInstaller])
       
        updateServiceName() : void
            def key  = "serviceName";
            def ps   = Context.Parameters;
            def name = if(ps.ContainsKey <| key) ps[key] else string.Empty
            unless(string.IsNullOrEmpty <| name)
                serviceInstaller.ServiceName = name
        
        public override Install(state : System.Collections.IDictionary) : void
            updateServiceName()
            base.Install(state)
            
        public override Uninstall(state: System.Collections.IDictionary) : void
            updateServiceName()
            base.Uninstall(state)
