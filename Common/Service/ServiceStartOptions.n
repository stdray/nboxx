﻿#pragma indent
using CommandLine
using CommandLine.Text

using Nemerle
using Nemerle.Collections
using Nemerle.Extensions
using Nemerle.Text
using Nemerle.Utility

using System
using System.Collections.Generic
using System.Linq

namespace NBoxx.Common.Service
    public class ServiceStartOptions
        
        public static Default : ServiceStartOptions
            get ServiceStartOptions()
                
        [Option('i', "install", Required = false, HelpText = "Install service")] \
        public Install   : bool
            get; set
            
        [Option('u', "uninstall", Required = false, HelpText = "Uninstall service")] \
        public Uninstall : bool 
            get; set
            
        [Option('r', "run", Required = false, HelpText = "Run service", DefaultValue = true)] \    
        public Run : bool
            get; set
            
        [HelpOption] \
        public GetUsage() : string
            HelpText() <-
                AdditionalNewLineAfterOption = true
                AddDashesToOption = true
                AddOptions(this)
            
        public InstallOrUninstall : bool
            get Install || Uninstall