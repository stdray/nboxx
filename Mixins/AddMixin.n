﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Compiler;
using Nemerle.Compiler.Parsetree;
using Nemerle.Compiler.Typedtree;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using PT = Nemerle.Compiler.Parsetree;
using TT = Nemerle.Compiler.Typedtree;
using SCG = System.Collections.Generic;

namespace NBoxx.Mixins {
    using MixinsHelper;

    [MacroUsage(MacroPhase.BeforeInheritance, MacroTargets.Class, Inherited = false, AllowMultiple = true)]
    macro AddMixin(typeBuilder : TypeBuilder, traitName : PExpr) {
        AddMixinImpl.DoTransform(Macros.ImplicitCTX(), typeBuilder, traitName)
    }
    [MacroUsage(MacroPhase.WithTypedMembers, MacroTargets.Class, Inherited = false, AllowMultiple = true)]
    macro AddMixin(typeBuilder : TypeBuilder, traitName : PExpr){
       AddMixinImpl.BuildCtors(Macros.ImplicitCTX(), typeBuilder, traitName)
    }

    module AddMixinImpl {
        
        public BuildCtors(typer : Typer, typeBuilder : TypeBuilder, traitName : PExpr) : void{
            def (traitDecl, _)        = ParseAddMixinDecl <| traitName;
            def mixinDef              = GetMixin(typer, traitDecl);
            def patchInfo             = MixinMessageBus.Instance.GetCtorPatch(mixinDef.FullName, typeBuilder.FullName);
            def patch                 = patchInfo.Patch;
            match(typeBuilder.GetConstructors()){
                | [] => typeBuilder.Define <| <[decl: public this() { $patch }]>
                | cs => cs.Cast.[MethodBuilder]().Iter(c => c.Body = <[ $(c.Body); $patch; ]>)
            }
        }
        
        public DoTransform(typer : Typer, typeBuilder : TypeBuilder, traitName : PExpr) : void {
            Macros.DefineCTX(typer);
            def (traitDecl, tyParams) = ParseAddMixinDecl <| traitName;
            def mixinDef              = GetMixin(typer, traitDecl);
            def req                   = GetRequirements <| mixinDef;
            def typeGenerics          = GetGenericArguments <| typeBuilder;
            def typeGeneticExprs      = typeGenerics.Map(p => <[$(p: name)]>);
            def thisTyParams          = if(tyParams is Some(ts)) ts  
                                        else typeGenerics;
            def reqTyParams           = GetGenericArguments <| req;
            def (_, revTyMap)         = GetGenericMappers(thisTyParams, reqTyParams);              
            def intTyParams           = reqTyParams.Select(revTyMap).Map(p => <[$(p: name)]>);
            def intDecl               = GetParametrizedDecl(req, intTyParams);
            typeBuilder.AddImplementedInterface(intDecl);
            def prov                  = GetProvides <| mixinDef;
            def traitField            = Macros.NewSymbol <| $"impl$(prov.Name)";
            def thisName              = Name <| typeBuilder.ParsedName;
            def selfTy                = if(typeGenerics.Length>0) <[ $(thisName: name)[..$(typeGeneticExprs)] ]>
                                        else                      <[ $(thisName: name) ]>;
            def mixinTy               = GetTypeName <| prov;
            def fieldDecl             = <[decl : private $(traitField : name) : $mixinTy[..$(selfTy :: intTyParams)] ; ]>;
            typeBuilder.Define(fieldDecl);
            def ctorPatch             = <[ $(traitField : name) = $mixinTy(this);]>;
            MixinMessageBus.Instance.RegisterCtorPatch(mixinDef.FullName, typeBuilder.FullName, ctorPatch);
           // AddToConstructors(typeBuilder, ctorPatch);
            def traitMembers          = prov.GetParsedMembers()
                                            .MapFiltered(IsMixinMembers, ImplMixinMember(traitField, revTyMap, _));
            traitMembers.Iter(typeBuilder.Define);
            typeBuilder.Compile();
            MixinMessageBus.Instance.Subscribe(typeBuilder.FullName, mixinDef.FullName, fun(mixinInt) {
                def mixinIntDecl      = GetParametrizedDecl(mixinInt, selfTy::intTyParams);
                typeBuilder.AddImplementedInterface(mixinIntDecl);
            });
        }
    }
}