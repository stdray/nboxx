﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Compiler;
using Nemerle.Compiler.Parsetree;
using Nemerle.Compiler.Typedtree;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;


namespace NBoxx.Mixins {
    using MixinsHelper;

    [MacroUsage(MacroPhase.BeforeInheritance, MacroTargets.Class)]
    macro DefMixin(typeBuilder : TypeBuilder) {
        DefMixinImpl.DoTransform(Macros.ImplicitCTX(), typeBuilder)
    }

    module DefMixinImpl {
        public DoTransform(typer : Typer, typeBuilder : TypeBuilder) : void {
            Macros.DefineCTX(typer);
            def prov               = GetProvides     <| typeBuilder;
            def tyParams           = GetGenericArguments <| prov;
            def selfTy             = tyParams.First().ToString();
            prov.Define <| <[ decl: private self : $(selfTy : dyn); ]>;
            def ctors             = GetConstructors <| prov;
            def isGenerated(m : ClassMember){
                def attrs = m.GetCustomAttributes();
                attrs.Any(a => a is <[MixinConstructorAttribute]>);
            }
            def userCtors = ctors.Filter(isGenerated >> _!);
            def (defautCtors, badCtors) = userCtors.Partition(c => c.header.Parameters.Length == 0);
            def alertBadCtors(ctors){
                when(ctors.Length> 0) {
                    def msg = "Mixin can not have constructor with parameters";
                    ctors.Iter(c => Message.Error(c.Location, msg));
                    throw InvalidOperationException(msg);
                }
            }
            alertBadCtors(badCtors);
            def defaultCtorCall = match(defautCtors) {
                | []   => <[();]>; 
                | [_]  => <[ this()]>;
                | _    => def msg = "Only one default constructor may be defined";
                defautCtors.Iter(c => Message.Error(c.Location, msg));
                Message.FatalError(msg);
            }
            prov.Define <| 
                <[ decl: 
                    [MixinConstructorAttribute]
                    public this(o : $(selfTy: dyn)) { 
                        self = o; 
                        $defaultCtorCall;               
                    } ]>;
            def makeMixinIface() {
                def req           = GetRequirements <| typeBuilder;
                def generics      = tyParams.Map(n => Splicable.Name(n) : Splicable);
                def baseIface     = req.ParsedName;
                def baseIfaceTys  = tyParams.Skip(1).Map(n => <[ $(n: name)]>);
                def constrains    = prov.AstParts.SelectMany(a => a.DeclaredTypeParameters.constraints).NToList();
                def mixinMembers  = prov.GetParsedMembers().MapFiltered(IsMixinMembers, MakeMixinIfaceMember);
                def mixinIface    =
                    if(req.DeclaredTypeParametersCount > 0) 
                        <[ decl:
                            [MixinInterfaceAttribute]
                            public interface Mixin[..$generics] : $(baseIface : name)[..$baseIfaceTys] where ..$constrains { 
                                ..$mixinMembers
                            }  ]>
                    else 
                        <[ decl: 
                            [MixinInterfaceAttribute]
                            public interface Mixin[..$generics] : $(baseIface : name) where ..$constrains { 
                                ..$mixinMembers
                            }  ]>;
                def curEnv = typeBuilder.ParsedDeclaration.GetEnv(typeBuilder.GlobalEnv);
                mixinIface.SetEnv(curEnv);
                def mi = typeBuilder.DefineNestedType(mixinIface);
                mi.Compile();
                mi
            }
            MixinMessageBus.Instance.Produce(typeBuilder.FullName, makeMixinIface());
        }
    }
}