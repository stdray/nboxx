﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;
using Nemerle.Compiler;
using Nemerle.Compiler.Parsetree;
using Nemerle.Compiler.Typedtree;

using System;
using System.Collections.Generic;
using System.Linq;

using PT = Nemerle.Compiler.Parsetree;
using TT = Nemerle.Compiler.Typedtree;
using SCG = System.Collections.Generic;


namespace NBoxx.Mixins {

    module MixinsHelper {
        public GetGenericArguments(tb : TypeBuilder) : list[Name] {
            tb.AstParts
                .Where(t => t.TypeParameters!=null)
                .SelectMany(t => t.TypeParameters.tyvars)
                .Map(t : Splicable => t.GetName())
        }
        public ExpandFullName(name : string) : PExpr {
            match(NList.ToList(name.Split(array['.']))) {
                | []              => <[]>
                | first :: others => others.FoldLeft(<[ $(first : usesite) ]>, 
                                                        (next, acc) => <[ $acc.$(next : usesite) ]>)
            }
        }
        public GetRequirements(tb : TypeBuilder) : TypeBuilder {
            def isGenerated(nestedType : TypeBuilder){
                def attrs = nestedType.GetModifiers().CustomAttributes;
                attrs.Any(a => a is <[MixinInterfaceAttribute]>);
            }
            match(tb.DeclaredNestedTypes.Filter(t => t.IsInterface && isGenerated(t)==false)) {
                | []  => Message.FatalError("Requirements are not specified");
                | [r] => r;
                | _   => Message.FatalError("All requirements must be specified in one place");
            }
        }
        public GetProvides(tb : TypeBuilder) : TypeBuilder {
            match(tb.DeclaredNestedTypes.Filter(t => t.IsInterface == false)) {
                | []  => Message.FatalError( "No members provided");
                | [r] => r;
                | _   => Message.FatalError("All provided members must be specified in one place");
            }
        }
        public ParseAddMixinDecl(decl : PExpr) : Name * option[list[Name]]{
            def namesFromExprs(ex) {
                ex.Map(e => if(e is <[ $(n : name)]>) n 
                            else Message.FatalError(e.Location, $"$e is not a name"));
            }
            match(decl) {
                | <[ $(n : name)          ]> => (n, None());
                | <[ $(n : name)[..$(ts)] ]> => (n, ts |> namesFromExprs |> Some)
                | _                          => Message.FatalError(decl.Location, "Incorrect mixin definition");
            }
        }
        public GetMixin(typer: Typer, traitName : Name) : TypeBuilder {
            def types = typer.Manager.NameTree.NamespaceTree.GetTypeBuilders(onlyTopDeclarations=true);
            match(types.Filter(t=>t.ParsedName.Equals(traitName))) {
                | []  => Message.FatalError(traitName.Location, $"Mixin $traitName not found");
                | [t] => t;
                | ts  => Message.FatalError(traitName.Location, "Conflict: " + string.Join(", ", ts.Select(_.FullName)));
            }
        }
        public GetTypeName(targetType: TypeBuilder) : PExpr {
            ExpandFullName <| targetType.FullName;
        }
        public GetParametrizedDecl(targetType: TypeBuilder, tyArgs : list[PExpr]) : PExpr {
            def typeName = ExpandFullName <| targetType.FullName;
            match(tyArgs) {
                | [] => typeName;
                | _  => def r = <[ $typeName[..$tyArgs]]>;
                        r;
            }
        }
        public GetConstructors(targetType: TypeBuilder): list[ClassMember.Function]{
            def pred(m : ClassMember){
                | PT.ClassMember.Function as f when f.Name == ".ctor" => true;
                | _                                                   => false;
            }
            def members = targetType.GetParsedMembers();
            members.MapFiltered(pred, m => m :> ClassMember.Function);
        }
        public MapTypeExpr(tyMap : Name -> Name, expr: PExpr) : PExpr {
            def map(e) {
                | <[ $(t: name)[..$ps] ]> => <[ $(tyMap(t) : name)[..$(ps.Map(map))]]>
                | <[ $t1 -> $t2]>         => <[ $(map(t1)) -> $(map(t2)) ]>
                | <[ $t1 * $t2 ]>         => <[ $(map(t1)) *  $(map(t2)) ]>
                | <[ $(t: name)]>         => <[ $(tyMap(t): name)]>;
                | _                       =>  e;
            }
            map(expr);
        }
        public IsMixinMembers(m: ClassMember) : bool {
            | PT.ClassMember.Property
            | PT.ClassMember.Function as f 
                when f.modifiers.Modifiers == NemerleModifiers.Public &&
                    f.modifiers.Modifiers != NemerleModifiers.Static &&
                     f.Name != ".ctor"                                      => true;
            | _                                                             => false;
        }
        public ImplMixinMember(field : Name, tyMap : Name -> Name, m : ClassMember) : ClassMember {
            def makeProp(prop : ClassMember.Property) : ClassMember{
                def sProp = prop.ParsedSplicableName.GetName();
                def type  = MapTypeExpr(tyMap, prop.returnType);
                match(prop.getter, prop.setter) {
                    | (Some, Some) => 
                        <[ decl : 
                            public $(sProp : name) : $type { 
                                public get { $(field:name).$(sProp : name)          } 
                                public set { $(field:name).$(sProp : name) = value  } } ]>;

                    | (Some, None) =>  
                        <[ decl : 
                            public $(sProp : name) : $type { 
                                public get { $(field:name).$(sProp : name) } } ]>;
                    | (None, Some) =>  
                        <[ decl : 
                            public $(sProp : name) : $type { 
                                public set { $(field:name).$(sProp : name) = value } } ]>;                                            
                    | _             => 
                            Message.FatalError("Invalid property without getter or setter");
                }
            }
            def makeFunc(func : ClassMember.Function) : ClassMember{
                def decl = func.header;
                def name = decl.ParsedSplicableName.GetName();
                def tys   = decl.TypeParameters.tyvars
                                   .Select(_.GetName())
                                   .Select(tyMap)
                                   .Map(r => Splicable.Name(r) : Splicable );
                def parms = decl.ParsedParameters;
                parms.Iter(pp => pp.Type = MapTypeExpr(tyMap, pp.Type));
                def retTy = MapTypeExpr(tyMap, decl.ParsedReturnType);
                def ppy   = parms.Map(p => <[ $(p.PName : name) ]>);
                <[decl: 
                    public $(name : name)[..$tys](..$parms) : $retTy {
                        $(field : name).$(name : name)(..$ppy); }]>;
            }
            match(m) {
                | PT.ClassMember.Property as p => p |> makeProp;
                | PT.ClassMember.Function as f => f |> makeFunc;
                | _                            => Message.FatalError($"$m is not supported");
            }
        }
        public MakeMixinIfaceMember(m : ClassMember) : ClassMember {
            def makeProp(prop: ClassMember.Property) {
                def sProp = prop.ParsedSplicableName.GetName();
                def type  = prop.returnType;
                match(prop.getter, prop.setter) {
                    | (Some, Some) => <[ decl : $(sProp : name) : $type { get; set; } ]>;
                    | (Some, None) => <[ decl : $(sProp : name) : $type { get;      } ]>;
                    | (None, Some) => <[ decl : $(sProp : name) : $type {       set } ]>;                                            
                    | _            => Message.FatalError("Invalid property without getter or setter");
                }
            }
            def makeFunc(func : ClassMember.Function) {
                def decl  = func.header;
                def name  = decl.ParsedSplicableName.GetName();
                def tys   = decl.TypeParameters.tyvars;
                def parms = decl.ParsedParameters;
                def retTy = decl.ParsedReturnType;
                <[decl: $(name : name)[..$tys](..$parms) : $retTy; ]>;
            }
            match(m) {
                | PT.ClassMember.Property as p => p |> makeProp;
                | PT.ClassMember.Function as f => f |> makeFunc;
                | _                            => Message.FatalError($"$m is not supported");
            }
        }
        public GetGenericMappers(fromArgs: Seq[Name], toArgs : Seq[Name]) : (Name -> Name) * (Name -> Name) {
            def mapArg(map, name) {
                def finded = map.Find <| name.ToString();
                if(finded is Some(v)) v
                else                  name
            }
            def forwardMap = Map <| fromArgs.Map2Lazy(toArgs, (f, t) => (f.ToString(), t));
            def reverseMap = Map <| toArgs.Map2Lazy(fromArgs, (t ,f) => (t.ToString(), f));
            (mapArg(forwardMap, _),  mapArg(reverseMap, _))
        }

    }
}
