﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Compiler;
using Nemerle.Compiler.Parsetree;
using Nemerle.Compiler.Typedtree;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;


namespace NBoxx.Mixins {
    [Record]
    public class MixinCtorPatch {
        public MixinFullName     : string;
        public TargetFullName    : string;
        public Patch             : PExpr;
    }

    [Record]
    public class SubcriberInfo {
        public SubcriberName  : string;
        public TargetFullName : string;
        public Callback       : TypeBuilder -> void;
    }

    [DesignPatterns.Singleton]
    public class MixinMessageBus {
        this() { }
        private subcribers : List[SubcriberInfo]             = List();
        private producers  : Dictionary[string, TypeBuilder] = Dictionary();
        private fieldInfos : List[MixinCtorPatch]             = List();

        public Produce(mixinFullName: string, tbInterface: TypeBuilder) : void {
            lock(producers)
                producers.Add(mixinFullName, tbInterface);
            lock(subcribers) 
                subcribers.Where(s => s.TargetFullName == mixinFullName).Iter(s => tbInterface |> s.Callback)
        }
        public Subscribe(subcriberName : string, targetFullName : string, callback : TypeBuilder -> void) : void {
            def subsriberInfo = SubcriberInfo(subcriberName, targetFullName, callback);
            lock(subcribers) 
                subcribers.Add <| subsriberInfo;
            lock(producers) 
                when(producers.ContainsKey(targetFullName))
                    callback <| producers[targetFullName]
        }

        public RegisterCtorPatch(mixinFullName: string, targetFullName : string, patch : PExpr) : void {
            lock(fieldInfos) 
                fieldInfos.Add <| MixinCtorPatch(mixinFullName, targetFullName, patch);
        }
        public GetCtorPatch(mixinFullName: string, targetFullName : string) : MixinCtorPatch {
            lock(fieldInfos) 
                fieldInfos.Single(fi => fi.TargetFullName == targetFullName && fi.MixinFullName == mixinFullName);
        }
    }
}
