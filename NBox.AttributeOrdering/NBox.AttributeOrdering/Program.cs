﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace AttrOrder {
    class Program {
        static void Main() {

            var yobas = new[] { 
                new Yoba { Age = 34, Name = "zxcew", Zikkurat = Tuple.Create(1, 8)   },
                new Yoba { Age = 1, Name = "asdsad", Zikkurat = Tuple.Create(-44, 8) }, 
                new Yoba { Age = 17, Name = "sdf" , Zikkurat = Tuple.Create(10, -3)  }, 
            };

            Console.WriteLine("Order by default {0} asc", YobaOrderMode.Zikkurat);
            foreach (var yoba in yobas.OrderByAttr(YobaOrder.T, YobaOrderMode.Zikkurat))
                Console.WriteLine(yoba);

            Console.WriteLine();

            Console.WriteLine("Order by {0} dasc", YobaOrderMode.Age);
            foreach (var yoba in yobas.OrderByAttr(YobaOrder.T, YobaOrderMode.Age))
                Console.WriteLine(yoba);

            Console.Read();
        }

    }


    #region Test entities

    public enum YobaOrderMode { Name, Age, Zikkurat }

    public class Yoba {
        [YobaOrder(YobaOrderMode.Name, IsDefault = true)]
        public string Name { get; set; }

        [YobaOrder(YobaOrderMode.Age)]
        public int Age { get; set; }

        [YobaOrder(YobaOrderMode.Zikkurat, Comparer = typeof(ZikkuratComparer))]
        public Tuple<int, int> Zikkurat { get; set; }

        public override string ToString() {
            return string.Format(
                "[Name {0}, Age = {1}, Zikkurat = {2}/{3}]",
                Name, Age, Zikkurat.Item1, Zikkurat.Item2);
        }
    }

    public class ZikkuratComparer : Comparer<Tuple<int, int>> {
        public override int Compare(Tuple<int, int> x, Tuple<int, int> y) {
            return Math.Abs(x.Item1 + x.Item2).CompareTo(Math.Abs(y.Item1 + y.Item2));
        }
    }

    public class YobaOrder : SmOrderAttribute, ISmOrder<YobaOrderMode> {
        public static YobaOrder T = null;
        public YobaOrder(YobaOrderMode order, bool isDefault = false, Type comparer = null)
            : base(order, isDefault, comparer) { }
    }

    #endregion


    public static class Htp {
        public static IEnumerable<T> OrderByAttr<T, TAttr, TEnum>(
            this IEnumerable<T> seq, TAttr t, TEnum order, bool asc = true)
            where TAttr : SmOrderAttribute, ISmOrder<TEnum>
            where TEnum : struct {
            return SmOrderer<T, TAttr, TEnum>.Default.Order(seq, order, asc);
        }
    }


    #region Attributes

    public interface ISmOrder<T> where T : struct { }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public abstract class SmOrderAttribute : Attribute {
        protected SmOrderAttribute(ValueType order, bool isDefault = false, Type comparer = null) {
            Order = order;
            IsDefault = isDefault;
            Comparer = comparer;
        }
        public ValueType Order { get; private set; }
        public bool IsDefault { get; set; }
        public Type Comparer { get; set; }
    }

    #endregion Attributes


    public class SmOrderer<T, TAttr, TEnum>
        where TAttr : SmOrderAttribute, ISmOrder<TEnum>
        where TEnum : struct {

        public static SmOrderer<T, TAttr, TEnum> Default = new SmOrderer<T, TAttr, TEnum>();

        readonly Dictionary<TEnum, IOrderer<T>> _orderers;
        readonly IOrderer<T> _default;

        private SmOrderer() {
            var orderers = GetOrderers();
            _default = orderers.Item1;
            _orderers = orderers.Item2;
        }

        public IEnumerable<T> Order(IEnumerable<T> seq, TEnum mode, bool asc) {
            IOrderer<T> orderer;
            if (_orderers.TryGetValue(mode, out orderer))
                return orderer.Order(seq, asc);
            if (_default != null)
                return _default.Order(seq, asc);
            throw new ArgumentException(
                string.Format("Order mode {0} is not supported, and default order not set", mode),
                "mode");
        }

        public static Tuple<IOrderer<T>, Dictionary<TEnum, IOrderer<T>>> GetOrderers() {
            var ty = typeof(T);
            const BindingFlags flags =
                BindingFlags.Instance | BindingFlags.Public |
                BindingFlags.GetProperty | BindingFlags.GetField;
            var memberOrderesQuery =
                from member in ty.GetMembers(flags)
                where member.IsDefined(typeof(TAttr), true)
                let memberType = GetMemberType(member)
                let attribute = member.GetCustomAttribute<TAttr>()
                let orderKey = (TEnum)attribute.Order
                let lambdaArg = Expression.Parameter(ty)
                let lambdaBody = Expression.PropertyOrField(lambdaArg, member.Name)
                let lambda = Expression.Lambda(lambdaBody, lambdaArg).Compile()
                let orderer = CreateOrderer(lambda, memberType, attribute.Comparer)
                select new { orderKey, orderer, attribute.IsDefault };
            var memberOrderers = memberOrderesQuery.ToList();
            var defaultOrdererQuery = memberOrderers.FirstOrDefault(o => o.IsDefault);
            var defaultOrderer = defaultOrdererQuery != null ? defaultOrdererQuery.orderer : null;
            var orderers = memberOrderers.ToDictionary(x => x.orderKey, x => x.orderer);
            return Tuple.Create(defaultOrderer, orderers);
        }

        static IOrderer<T> CreateOrderer(Delegate @delegate, Type returnType, Type comparerType) {
            var type = typeof(Orderer<,>);
            var typeArgs = new[] { typeof(T), returnType };
            var objType = type.MakeGenericType(typeArgs);
            var comparer = comparerType != null ? Activator.CreateInstance(comparerType) : null;
            var obj = Activator.CreateInstance(objType, new object[] { @delegate, comparer });
            return (IOrderer<T>)obj;
        }

        static Type GetMemberType(MemberInfo member) {
            var prop = member as PropertyInfo;
            if (prop != null)
                return prop.PropertyType;
            var field = member as FieldInfo;
            if (field != null)
                return field.FieldType;
            throw new ArgumentException(
                string.Format("Member {0} is not supported", member.GetType().Name),
                "member");
        }
    }

    public interface IOrderer<T> {
        IOrderedEnumerable<T> Order(IEnumerable<T> items, bool asc);
    }

    public class Orderer<T, TKey> : IOrderer<T> {
        readonly Func<T, TKey> _orderFunc;
        readonly IComparer<TKey> _comparer;

        public Orderer(Func<T, TKey> orderFunc, IComparer<TKey> comparer) {
            _orderFunc = orderFunc;
            _comparer = comparer;
        }

        public IOrderedEnumerable<T> Order(IEnumerable<T> items, bool asc) {
            if (_comparer != null)
                return asc
                    ? items.OrderBy(_orderFunc, _comparer)
                    : items.OrderByDescending(_orderFunc, _comparer);
            return asc
                ? items.OrderBy(_orderFunc)
                : items.OrderByDescending(_orderFunc);
        }
    }
}
