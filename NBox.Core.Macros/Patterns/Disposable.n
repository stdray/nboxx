﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;
using Nemerle.Compiler;
using Nemerle.Compiler.Parsetree;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NBoxx.Common.Patterns {
    [MacroUsage(MacroPhase.BeforeInheritance, MacroTargets.Class)]
    macro Disposable(typeBuilder : TypeBuilder, _ = null, _=null, _=null)  {
        typeBuilder.AddImplementedInterface(<[ System.IDisposable ]>);
    }

    [MacroUsage(MacroPhase.BeforeTypedMembers, MacroTargets.Class)]
    macro Disposable(typeBuilder : TypeBuilder, 
                        disposeManaged = <[ () ]>, 
                        disposeUnmanaged = <[ () ]>, 
                        disposeName = <[ Dispose ]>) {
        DisposableImpl.DoTransform(Macros.ImplicitCTX(), typeBuilder,
                                    disposeManaged, disposeUnmanaged, 
                                    disposeName)
    }

    module DisposableImpl  {
        public DoTransform(typer : Typer, typeBuilder : TypeBuilder, 
                            disposeManaged : PExpr, disposeUnmanaged : PExpr, 
                            disposeName : PExpr) : void {
            Macros.DefineCTX(typer); // это нружно для цитаты <[ ttype: System.IDisposable ]>
            def needUnmanagedDispose = ! (disposeUnmanaged is <[ () ]>);
            def iDisposableType = <[ ttype: System.IDisposable ]>;
            def needOverride = typeBuilder
                .BaseClass
                .TryRequire(iDisposableType);
            //def defineMember(ast) { typeBuilder.Define(ast) }
            def defineMember(ast) { 
                _ = typeBuilder.DefineWithSource(ast) 
            }
            //Message.Hint($"needOverride=$needOverride");
            def attrs = Modifiers(NemerleAttributes.Private, []);
            _ = <[ decl: ..$attrs public class MyClass { } ]>;
            // Добавляем метод Dispose. Метод помечаем макроатрибутом ImplementDisposeFields.
            // Он "вызовится" позже.
            defineMember(<[ 
                decl: [RecordIgnore] private mutable _disposed : bool; 
            ]>);
            def disposeIDisposableFields = Macros
                .NewSymbol("DisposeIDisposableFields");
            defineMember(<[ 
                decl: [ImplementDisposeFields]
                    private $(disposeIDisposableFields : name)() : void { } 
            ]>);
            def modifiers =
                if (needOverride) 
                    NemerleModifiers.Protected | NemerleModifiers.Override
                else 
                    NemerleModifiers.Protected | NemerleModifiers.Virtual;
            def attrsAndMods = AttributesAndModifiers(modifiers, []);
            def baseCall = 
                if (needOverride) 
                    <[ base.Dispose(disposing); ]> 
                else <[ () ]>;
            defineMember(<[ 
                decl: ..$attrsAndMods Dispose(disposing : bool) : void {
                    unless (_disposed) {
                        when (disposing) {
                            // Генерируем вызовы Dispose для IDisposable-полей.
                            $(disposeIDisposableFields : name)();
                            // Вставояем код очистки управляемых ресурсов предосталяемый пользователем.
                            $disposeManaged;
                        }
                        // Вставляем код очистки неуправляемых ресурсов предосталяемый пользователем.
                        $disposeUnmanaged;
                        // TODO: Обнуляем все изменяемые поля.
                        $baseCall;
                        _disposed = true;
                    }
            } ]>);
            when (needUnmanagedDispose)
                defineMember(<[ 
                    decl: protected override Finalize() : void { 
                        Dispose(false); 
                    } 
                ]>);
            unless (needOverride) {
                def disposeMethodName = match (disposeName) {
                    | <[ $(_ : name) ]> as nameRef => nameRef
                    | _ => Message.Error(disposeName.Location, 
                                        "Expected simple name");
                            <[ Dispose ]> 
                };
                defineMember(<[ 
                    decl: public $(disposeMethodName : name)() : void 
                            implements IDisposable.Dispose {
                                Dispose(true);
                                GC.SuppressFinalize(this);
                    } 
                ]>);
            }
        }
    }
    
     [MacroUsage(MacroPhase.WithTypedMembers, MacroTargets.Method)]
      macro ImplementDisposeFields(typeBuilder : TypeBuilder, 
                                   method : MethodBuilder) {
        ImplementDisposeFieldsImpl.DoTransform(
            Macros.ImplicitCTX(), typeBuilder, method)
      }

      module ImplementDisposeFieldsImpl {
        public DoTransform(typer : Typer, typeBuilder : TypeBuilder, 
                           method : MethodBuilder) : void {
            Macros.DefineCTX(typer); // это нружно для цитаты <[ ttype: System.IDisposable ]>
            // Так надо получать описание типа в макросах. Использовать System.Type в макросах нельзя!
            def iDisposableType = <[ ttype: System.IDisposable ]>;
            def isMemberTypeImplementIDisposable(member) : bool {
                // Вызвать Dispose() имеет смысл только у полей и свойств определенных в текущем классе. Фильтрум их...
                member is IField
                // Получаем тип члена и проверям, что он унифицируется с IDisposable. А использовать System.Type в макросах нельзя!
                && member.GetMemType().TryRequire(iDisposableType)
            }
            // Получаем все экземплярные поля и свойства типы которых реализуют IDisposable.
            def flags = BindingFlags.DeclaredOnly | BindingFlags.Instance
                      | BindingFlags.Public | BindingFlags.NonPublic;
            def members = typeBuilder
                .GetMembers(flags)
                .Filter(isMemberTypeImplementIDisposable);
            // Преобразуем список членов в список выражений проиводящий обращение к ним и вызвающих от них Dispose().
            // Оператор ?. позволяет защититься от вызова по нулевому указателю.
            def exprs = members.Map(m => <[ 
                (this.$(m.Name : usesite) : System.IDisposable)?.Dispose() 
            ]>);
            // Заменяем тело Dispose-а данного класа. Цитата ..$х раскрывает список выражений.
            method.Body = <[ { ..$exprs } ]>;
        }
    }
}
