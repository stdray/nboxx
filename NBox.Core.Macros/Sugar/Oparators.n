﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Compiler.Parsetree;
using Nemerle.Text;
using Nemerle.Utility;


using System;
using System.Collections.Generic;
using System.Linq;

namespace NBoxx.Core.Sugar {
    macro LockWhen(lockObj, cond, body)
    syntax("lockWhen", "(", lockObj, ";", cond, ")", body) {
        <[ when($cond) lock($lockObj) when($cond) $body; ]>
    }
    macro JustTry(body)
    syntax("justTry", body) {
        <[ try { $body } catch { | _ => () } ]>
    }
}
    