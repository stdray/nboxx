﻿using Nemerle;
using Nemerle.Compiler;
using Nemerle.Compiler.Parsetree;

using System;

namespace NBoxx.NameConventions {
    using RuleUtils;
    using СonditionOperators;
    
    [MacroUsage(MacroPhase.BeforeInheritance, MacroTargets.Assembly)] 
    macro CheckStyle(alertType : string) {
        CheckStyleImpl.Transform(Macros.Manager(), alertType);
    }
    module CheckStyleImpl {
        public Transform(manager : ManagerClass, alertType : string) : void {
            def alert = Alert(alertType |> ParseAlertMode, _, _);
            def typeRules = [
                TypeRule(cond = !(TypeName >> FstChar >> (Char.IsUpper & Char.IsLetter)),
                         eval = t => alert(t.Location, $"Type name '$(t.Name)' must begin with a capital letter")),
                TypeRule(cond = IsInterface & !(TypeName >> FstChar >> (_ == 'I')),
                         eval = t => alert(t.Location, $"Interface name '$(t.Name)' must begin with 'I' letter")),
            ];
            def memberRules = [
                MemberRule(cond = !IsCtor & (IsPublic | IsProtected) & !(MemberName >> IsFstUpper) & (MemberName >> IsFstLetter), 
                           eval = m => alert(m.Location, $"Member name '$(m.Name)'" + "must begin with a capital letter")),
                MemberRule(cond = !IsCtor & IsPrivate & !(MemberName >> IsFstLower),
                           eval = m => alert(m.Location, $"Member name '$(m.Name)' must begin with a lowercase letter")),
                MemberRule(cond = IsPrivate & IsMutable & !(MemberName >> IsFstUnderscore),
                           eval = m => alert(m.Location, $"Name of private mutable member '$(m.Name)' must begin with a _ letter")),
            ];
            def types = AllTypes(manager);
            RunRules(typeRules, types);
            def members = ToMembers(types);
            RunRules(memberRules, members);
        }
    }
}