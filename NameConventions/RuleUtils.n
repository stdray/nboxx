﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Compiler;
using Nemerle.Compiler.Parsetree;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

namespace NBoxx.NameConventions {
    type NM = NemerleModifiers;
    using СonditionOperators;

    module RuleUtils {
        public FstChar             : string -> char                = s => s[0];
        public IsFstUpper          : string -> bool                = FstChar >> Char.IsUpper;
        public IsFstLower          : string -> bool                = FstChar >> Char.IsLower;
        public IsFstLetter         : string -> bool                = FstChar >> Char.IsLetter;
        public IsFstUnderscore     : string -> bool                = FstChar >> (_ == ' ');
        public HasAccessModifiers(m : ClassMember) : bool{
            m.modifiers.Modifiers & NM.AccessModifiers != NM.None
        }
        public IsCtor              : ClassMember -> bool           = m => m.Name == ".ctor";
        public IsPublic            : ClassMember -> bool           = m => m.modifiers.Modifiers & NM.Public != NM.None;
        public IsPrivate           : ClassMember -> bool           = m => m.modifiers.Modifiers & NM.Private != NM.None;
        public IsProtected         : ClassMember -> bool           = m => m.modifiers.Modifiers & NM.Protected != NM.None;
        public IsInternal          : ClassMember -> bool           = m => m.modifiers.Modifiers & NM.Internal != NM.None;
        public IsProtectedInternal : ClassMember -> bool           = IsProtected & IsInternal;
        public IsMutable           : ClassMember -> bool           = m => m.modifiers.Modifiers & NM.Mutable != NM.None;
        public IsInterfaceMember(m : ClassMember) : bool {
            m.DefinedIn.TypeBuilder.IsInterface;
        }
        public IsInterface         : TypeBuilder -> bool           = _.IsInterface;
        public MemberName          : ClassMember -> string         = _.Name;
        public TypeName            : TypeBuilder -> string         = _.Name;
        public HasAccessModifiers(t :  TypeBuilder) : bool { 
            t.ParsedDeclaration.modifiers.Modifiers & NM.AccessModifiers != NM.None
        }
        public SetModifier(mod : NM, m : ClassMember) : void { 
            m.modifiers.Modifiers |= mod 
        }
        public SetModifier(mod : NM, t : TypeBuilder) : void { 
            t.Ast.Attributes |= mod;
        }
        public RunRules[T](rules : Seq[StyleRule[T]], items : Seq[T]) : void {
            foreach(item in items)
                foreach(rule in rules)
                    when(item |> rule.Cond)
                        item |> rule.Eval;
        }
        public Alert(mode : AlertMode, loc : Location, msg : string) : void {
            def alert = match(mode) {
                | Error   => (l, m) => Message.FatalError(l, m);
                | Warning => Message.Warning;
                | Info    => Message.Hint;
            }
            alert(loc, msg);
        }
        public ParseAlertMode(mode : string) : AlertMode {
            match(mode.Trim().ToLower()) {
                | "error"    => Error;
                | "warning"  => Warning;
                | "info"     => Info;
                | p          => 
                        Message.FatalError($"Incorrect parameter $p. 'Error', 'Warning' or 'Info' expected")
            }
        }
        public AllMembers : ManagerClass -> Seq[ClassMember] = m => 
            m.NameTree.NamespaceTree.GetTypeBuilders().SelectMany(_.GetParsedMembers());
        public AllTypes   : ManagerClass -> Seq[TypeBuilder] = m => 
            m.NameTree.NamespaceTree.GetTypeBuilders();
        public ToMembers  : Seq[TypeBuilder] -> Seq[ClassMember] = ts => 
            ts.SelectMany(_.GetParsedMembers());
    }
}
