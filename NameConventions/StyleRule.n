﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Compiler;
using Nemerle.Compiler.Parsetree;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;


namespace NBoxx.NameConventions {
    [Record] 
    class StyleRule[T] {
        public Cond     : T -> bool;
        public Eval     : T -> void;
    }
    
    type MemberRule      = StyleRule[ClassMember];
    
    type TypeRule        = StyleRule[TypeBuilder];
    
    module СonditionOperators {
        public static @|[T](p1 : T -> bool, p2 : T -> bool) : T -> bool {
            x => p1(x) || p2(x)
        }
        public static @&[T](p1 : T -> bool, p2 : T -> bool) : T -> bool {
            x => p1(x) && p2(x)
        }
        public static @![T](p1 : T -> bool) : T -> bool {
            x => !p1(x)
        }
    }
    
    enum AlertMode { | Error | Warning | Info }
}
